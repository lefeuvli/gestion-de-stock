export class Produit {
    nom: string;
    fournisseur: string;
    email: string;
    ingredients: string[];
    description: string;
    age: number;
    conservation: string;
    prix: number;

    constructor(nom: string, fournisseur: string, email: string, ingredients: string[], description: string, age: number, conservation: string, prix: number) {
		this.nom = nom;
		this.fournisseur = fournisseur;
		this.email = email;
		this.ingredients = ingredients;
		this.description = description;
		this.age = age;
		this.conservation = conservation;
		this.prix = prix;
	}
}