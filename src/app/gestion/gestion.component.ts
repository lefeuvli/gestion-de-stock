import { Component, OnInit } from '@angular/core';
import { ProduitService } from '../produit.service';
import { Produit } from '../model';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-gestion',
  templateUrl: './gestion.component.html',
  styleUrls: ['./gestion.component.css']
})
export class GestionComponent implements OnInit {
  tabProduit: Produit[];
  formulaireBouton = "Ajouter"
  hiddenFormulaire = true;
  produitForm: FormGroup;
  nomCtrl: FormControl;
  fournisseurCtrl: FormControl;
  ageCtrl: FormControl;
  descriptionCtrl: FormControl;

  constructor(public serviceProduit: ProduitService, public fb: FormBuilder) {
    this.nomCtrl = fb.control('', [Validators.required]);
    this.fournisseurCtrl = fb.control('', [Validators.required]);
    this.ageCtrl = fb.control('', [Validators.required, Validators.pattern("[0-9]*")]);
    this.descriptionCtrl = fb.control('', [Validators.required]);
    this.produitForm = fb.group({
			nom: this.nomCtrl,
			fournisseur: this.fournisseurCtrl,
			age: this.ageCtrl,
			description: this.descriptionCtrl
	 	});
  }

  ngOnInit() {
    this.tabProduit = this.serviceProduit.list()
  }

  supprimer(index) {
    this.serviceProduit.deleteProduit(index);
  }

  addProduit() {
    var produit = new Produit(
      this.produitForm.value.nom,
      this.produitForm.value.fournisseur,
      "",
      [],
      this.produitForm.value.description,
      this.produitForm.value.age,
      "",
      0
    );
    this.tabProduit = this.serviceProduit.addProduit(this.produitForm.value);
    this.initForm();
    this.voirFormulaire();
  }

  voirFormulaire() {
    this.hiddenFormulaire = !this.hiddenFormulaire;
    if (this.hiddenFormulaire) {
      this.formulaireBouton = "Ajouter";
      this.initForm();
    } else {
      this.formulaireBouton = "Annuler";
    }
  }

  initForm() {
    this.produitForm.reset();
  }

}
