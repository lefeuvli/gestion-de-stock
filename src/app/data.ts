import { Produit } from "./model";

export let tabProduit: Array<Produit> = [
    new Produit("produit0","fournisseur0","email0",["ingredient-1", "ingredient0", "ingredient1"],"description,hjkfsdbbsd",10,"conservation",2000),
    new Produit("produit1","fournisseur1","email1",["ingredient0", "ingredient1", "ingredient2"],"description1, sdhjbfkfhbjcx",10,"conservation1",2130),
    new Produit("produit2","fournisseur2","email2",["ingredient1", "ingredient2", "ingredient3"],"description2, sdjhfsknchjxvg",10,"conservation2",53),
    new Produit("produit3","fournisseur3","email3",["ingredient2", "ingredient3", "ingredient4"],"description3, svjhdjsknxc",10,"conservation3",6430),
    new Produit("produit4","fournisseur4","email4",["ingredient3", "ingredient4", "ingredient5"],"description4, sghsdjkbcxjh",10,"conservation4",743),
]
