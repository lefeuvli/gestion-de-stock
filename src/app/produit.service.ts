import { Injectable } from '@angular/core';
import { tabProduit } from './data';
import { Produit } from './model';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  constructor() { }

  list() {
    return tabProduit;
  }

  addProduit(produit) {
    tabProduit.push(produit);
    return tabProduit;
  }

  deleteProduit(index) {
    tabProduit.splice(index, 1);
  }
}
